//
//  ViewController.swift
//  catfetcher
//
//  Created by David Cumming on 7/7/19.
//  Copyright © 2019 davecumming. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    struct Cat: Decodable {
        let url: String
        let breeds : [String]
        let width : Int
        let height : Int
    }
    
    @IBAction func buttonPressed(_ sender: Any) {
        guard let url = URL(string:"https://api.thecatapi.com/v1/images/search")
            else {
                return
        }

        let session = URLSession.shared
        let urlRequest = URLRequest(url: url)

        let task = session.dataTask(with: urlRequest, completionHandler: {
            (data, response, error) in

            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            if let cats = try? JSONDecoder().decode([Cat].self, from: responseData){
                let url = URL(string: cats[0].url)
                let data = try? Data(contentsOf: url!)
                DispatchQueue.main.async{
                    self.label.text = cats[0].url;
                    self.imageView.image = UIImage(data:data!)
                }
            }
        })
        task.resume()

    }
}

